import tkinter as tk
from tkinter.filedialog import *
import numpy as np
import librosa as lr
import librosa.display as ld
import matplotlib.pyplot as plt
class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("voice")
        self.resizable(0, 0)
        self.text = tk.Text(self, width=50, height=10)
        self.btn_clear = tk.Button(self, text="Очистить",
                                   command=self.clear_text)
        self.btn_insert1 = tk.Button(self, text="duration",
                                    command=self.insert_text)
        self.btn_insert2 = tk.Button(self, text="SR",
                                    command=self.insert_text_2)
        self.btn_insert3 = tk.Button(self, text="plot",
                                    command=self.plot)
        self.btn_insert4 = tk.Button(self, text="plot_фурье",
                                    command=self.plot_fu)
        self.text.pack()
        self.btn_clear.pack(side=tk.LEFT, expand=True, pady=10)
        self.btn_insert1.pack(side=tk.LEFT, expand=True, pady=10)
        self.btn_insert2.pack(side=tk.LEFT, expand=True, pady=10)
        self.btn_insert3.pack(side=tk.LEFT, expand=True, pady=10)
        self.btn_insert4.pack(side=tk.LEFT, expand=True, pady=10)
        self.op = askopenfilename()
    def clear_text(self):
        self.text.delete("1.0", tk.END)
    def dur(self):
        x,sr = lr.load(self.op)
        return len(x)/sr
    def insert_text(self):
        self.text.insert(tk.INSERT, f'{self.dur()} ')
    def inform(self):
        x,sr = lr.load(self.op)
        return sr
    def insert_text_2(self):
        self.text.insert(tk.INSERT, f'{self.inform()} ')
    def plot_fu(self):
        x, sr = lr.load(self.op, sr=44100)
        X = lr.stft(x)
        Xdb = lr.amplitude_to_db(abs(X))
        plt.figure(figsize=(10, 5))
        ld.specshow(Xdb, sr=sr, x_axis='time', y_axis='hz')
        plt.colorbar()
        plt.show()

    def plot(self):
        y, sr = lr.load(format(self.op))
        time = np.arange(0, len(y)) / sr
        fig, ax = plt.subplots()
        ax.plot(time, y)
        ax.set(xlabel='Time(s)', ylabel='sound amplitude')
        plt.show()

if __name__ == "__main__":
    app = App()
    app.mainloop()